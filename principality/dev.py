from pathlib import Path
from nextcord.ext import tasks

from principality import Principality
from cog import Cog, get_cogs
from core.overseer import Overseer

class PrincipalityDev(Principality):

    found_cogs = {}
    processed_cogs = []

    async def on_ready(self):

        await super().on_ready()
        self.config_overseer = Overseer(self.config.config_directory, '*.toml')
        self.cog_overseer = Overseer(Path(self.config.cog_directory), ['*.py', '*.toml', '*.json'])
        await self.update_checker.start()

    @tasks.loop(seconds=5)
    async def update_checker(self):

        self.config_overseer.check()
        # check configs
        for file in self.config_overseer.changes.changed:
            cog_id = file.stem
            cog = self.cogs.get(cog_id, None)
            if not cog:
                print("Non-Cog configuration file edited, you'll have to restart your bot manually to apply changes")
                continue
            cog.reload_config()
            print(f"Reloaded config for cog '{cog_id}'")

        # check cogs
        self.cog_overseer.check()
        added, changed, removed = self.cog_overseer.changes

        to_remove = []
        for file in removed:
            if file.name != 'pyproject.toml':
                continue
            cog = self.cogs[file.parent.stem]
            to_remove.append(cog)
            removed = {r for r in removed if not r.is_relative_to(file.parent)}

        to_add = []
        for file in added:
            if file.name != 'pyproject.toml':
                continue
            cog = Cog.from_dir(file.parent)
            cog = cog()
            to_add.append(cog)
            added = {a for a in added if not a.is_relative_to(file.parent)}

        to_reload = []
        files = changed.union(added).union(removed)
        for file in files:
            # find cog id and cache it
            cog = await self.find_cog(file)
            if not cog:
                self.logger.warning(f"Couldn't find what cog '{file}' belongs to")
                continue

            if cog in self.processed_cogs:
                continue

            to_reload.append(cog)

            self.processed_cogs.append(cog)
        
        if to_add:
            self.logger.pending('Adding cogs...')
            for cog in to_add:
                try:
                    self.add_cog(cog)
                    self.load_cog(cog)
                    await self.async_load_cog(cog)
                    await self.sync_application_commands()
                    self.logger.done(cog.name)
                except Exception as error:
                    self.logger.error(cog.name)
                    self.print_exception(cog.name, error, cog.id)
                    self.unload_cog(cog)
            print('')

        if to_remove:
            self.logger.pending('Removing cogs...')
            for cog in to_remove:
                try:
                    await self.unload_cog(cog)
                    await self.sync_application_commands()
                    self.logger.done(cog.name)
                except Exception as error:
                    self.logger.error(cog.name)
                    self.print_exception(cog.name, error, cog.id)
                    self.unload_cog(cog)
            print('')

        if to_reload:
            self.logger.pending('Reloading cogs...')
            for cog in to_reload:
                try:
                    await self.reload_cog(cog)
                    self.logger.done(cog.name)
                except Exception as error:
                    self.logger.error(cog.name)
                    self.print_exception(cog.name, error, cog.id)
                    self.unload_cog(cog)
            print('')

    async def find_cog(self, path: Path) -> Cog:
        cog_dir = self.found_cogs.get(path, None)
        if not cog_dir:
            if path.name == 'pyproject.toml':
                cog_dir = path.parent
            else:
                cog_dir = self._find_cog_dir(path)
                if not cog_dir:
                    return None

        self.found_cogs[path] = cog_dir
        return Cog.from_dir(cog_dir)

    def _find_cog_dir(self, path: Path) -> str:
        if path == Path('.'):
            return None
        files = [i for i in path.glob('pyproject.toml')]
        if len(files) < 1:
            return self._find_cog_dir(path.parent)
        if len(files) > 1:
            return None
        return files[0].parent