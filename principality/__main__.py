from os import getenv
from asyncio import run
from nextcord.errors import PrivilegedIntentsRequired, LoginFailure
from typer import Option
from aiohttp import ClientConnectionError

from core.cli import CLI
from principality import Principality
from utils import url_to_json

app = CLI()

def get_token(client = Principality(load_cogs=False)):
    return getenv(client.config.token_env_var)

def get_id(client = Principality(load_cogs=False)):
    return url_to_json('https://discordapp.com/api/oauth2/applications/@me', {'Authorization': 'Bot '+get_token(client)})['id']

@app.command()
def invite():
    app.logger.done(f"Your Bot's invite link is: https://discord.com/api/oauth2/authorize?client_id={get_id()}&permissions=8&scope=bot")
    #app.logger.warning("You should ")

@app.command()
def start(
    dev_mode: bool = Option(False, "--dev", help="Start the bot in Development Mode"),
    config_file: str = Option('config.toml', "--config", help="Choose which bot configuration file to use"),
    ):
    """Start your Principality Bot"""

    print('')
    if dev_mode:
        from principality.dev import PrincipalityDev
        client = PrincipalityDev
        app.logger.pending('Starting Principality in Development Mode...\n')
    else:
        client = Principality
        app.logger.pending('Starting Principality...\n')
    client = client(verbose=app.logger.verbose, config_file=config_file)

    token = getenv(client.config.token_env_var)
    if not token:
        app.logger.info("To get started, you have to provide a Discord Bot token")
        app.logger.info("If you have not made a Discord Bot or do not know how to get its token, follow this guide:")
        app.logger.info("https://discordpy.readthedocs.io/en/stable/discord.html")
        app.logger.info("")
        app.logger.info("This token is stored in the .env file, for future use")
        app.logger.info("In case you enter an invalid token, you can change it inside of the .env file by replacing the text after TOKEN=\n")
        token = input("Token: ")
        print('')
        if not token: return print("A Discord Bot token must be provided in order to start.")

        with open('.env', 'r') as file:
            env = file.read().split('\n')
        for val in env:
            if val.startswith('TOKEN='): env[env.index(val)] = 'TOKEN=' + token
        with open('.env', 'w') as file:
            file.write('\n'.join(env))

    try:
        run(client.run(token))
    except PrivilegedIntentsRequired:
        app.logger.info("Priviledged Gateway Intents have not been enabled for your bot, enable them so your bot can properly initialize")
        app.logger.info("You can enable them here:")
        app.logger.info(f"https://discord.com/developers/applications/{get_id()}/bot")
    except LoginFailure:
        app.logger.warning("You have provided an improper Discord Bot token, you can replace it with a proper one inside of the .env file")
    except ClientConnectionError:
        app.logger.warning("Cannot connect to Discord. Check your internet connection?")
    except (KeyboardInterrupt, ValueError):
        app.logger.done("Shutting Down!")
        return

@app.command()
def info():
    """W.I.P."""



def main():
    app()

if __name__ == '__main__':
    main()
