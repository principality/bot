from asyncio import run
from pathlib import Path
from nextcord import AllowedMentions, Intents
from nextcord.ext.commands import Bot
from typing import Literal
from pyfigure import Configurable, Option
from nextcord.ext import tasks
from copy import deepcopy

import traceback

from cog import Cog, get_cogs
from core import Core
from core.logger import Logger

class Principality(Bot, Core):

    cogs_loaded = False
    cogs_ready = False

    def __init__(self,
        config_file: Path = Path('config.toml'),
        verbose: bool = False,
        load_cogs: bool = True,
        ready_cogs: bool = True,
        *args
    ):

        self._ready_cogs = ready_cogs

        Core.__init__(self, verbose, config_file)
        Bot.__init__(self, rollout_all_guilds=True, intents=Intents.all(), allowed_mentions=AllowedMentions(everyone=False, replied_user=False), *args)

        if load_cogs: self.load_cogs()

    def load_cogs(self):
        cogs = get_cogs(Path(self.config.cog_directory))
        if not cogs: return self.logger.warning("No cogs to load.\n")
        self.logger.pending('Loading Cogs...')
        iter_cogs = cogs.copy().values()

        for cog in iter_cogs:
            try:
                self.add_cog(cog, override=True)
                self.load_cog(cog)
                self.logger.done(cog.name)
            except Exception as error:
                self.logger.error(cog.name)
                self.print_exception(cog.name, error, cog.id)
                self.unload_cog(cog)

        self.cogs_loaded = True
        self.logger.done('Cogs loaded!\n')

    async def on_ready(self):
        if not self._ready_cogs: return
        if not self.cogs: return self.logger.warning("No cogs to ready.\n")
        self.logger.pending('Readying Cogs...')
        iter_cogs = self.cogs.copy().values()

        for cog in iter_cogs:
            try:
                await self.async_load_cog(cog)
                self.logger.done(cog.name)
            except Exception as error:
                self.logger.error(cog.name)
                self.print_exception(cog.name, error, cog.id)
                self.unload_cog(cog)

        self.cogs_ready = True
        self.logger.done('Cogs ready!\n')

    def load_cog(self, cog: Cog) -> bool:
        cog.load(**{'bot': self})

    async def async_load_cog(self, cog: Cog) -> bool:
        await cog.ready(**{'bot': self})

    def unload_cog(self, cog: Cog) -> bool:
        old_cog = self.cogs[cog.id]
        self.remove_cog(cog.id)
        del old_cog
    
    async def reload_cog(self, cog: Cog) -> bool:
        self.unload_cog(cog)
        self.add_cog(cog, override=True)
        await self.sync_all_application_commands()
        self.load_cog(cog)
        await self.async_load_cog(cog)
    
    def print_exception(self, message: str, error: Exception, file_split: str = None):
        error_print = self.logger.error(message)
        data = traceback.TracebackException.from_exception(error).stack[1]
        file = data.filename
        print(file)
        if file_split:
            file = file_split + file.split(file_split, 1)[1]
        error_print.add(f'Exception in "{file}" on line {data.lineno}:')
        error_print.add(f"    {data.line}")
        error_print.add(f"{type(error).__name__}: {error}")
        self.unload_cog(cog)