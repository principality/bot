from pathlib import Path
from superdict import SuperDict
from importlib import reload
from importlib.util import spec_from_file_location, module_from_spec
from inspect import isclass, getmembers
from typing import List

import nextcord
from nextcord.ext.commands import Cog as DiscordCog
from tomlkit import load
from pyfigure import Configurable, Option
from core import get_core
from core.logger import Logger
from core.databases import Database, get_db

core = get_core()

class ConfigOption(Option):
    pass

class SlashOption(nextcord.SlashOption):
    pass

class Cog(DiscordCog):

    name: str
    id: str
    metadata: SuperDict
    directory: Path
    config_file: Path
    db: Database
    logger: Logger

    slash_command = nextcord.slash_command
    command = slash_command
    user_command = nextcord.user_command
    message_command = nextcord.message_command
    command = nextcord.ext.commands.command

    @classmethod
    def from_dir(cls, path: Path):
        spec = spec_from_file_location(path.stem, path / 'src' / (path.stem+'.py'))
        module = module_from_spec(spec)
        spec.loader.exec_module(module)
        cog = _cog_from_module(module)
        return cog(path / 'src')

    def __init__(self, directory = Path):
        self.directory = directory

        # read metadata
        project_file = self.directory.parent / 'pyproject.toml'
        with open(project_file, 'r') as file:
            self.metadata = SuperDict(load(file))
            project = self.metadata.pop('project')
            self.metadata.update(project)

        # setup data
        self.name = self.metadata.name
        self.id = self.directory.parent.stem
        self.__cog_name__ = self.id
        self.config_file = core.config.config_directory / (self.id+'.toml')
        self.db = get_db(core.config.default_database_type, self.name)

    def load(self, bot):
        """Execute code before the bot is connected to discord."""

    async def ready(self, bot):
        """Execute code after the bot has connected to discord. (Quick method for the `on_ready` listener)"""

    def unload(self, bot):
        """Executes code right before the cog gets unloaded."""

def get_cogs(path: Path) -> List[Cog]:
    cogs = {}
    for file in path.rglob('*/pyproject.toml'):
        cog = Cog.from_dir(file.parent)
        cogs[file.parent.stem] = cog
    return cogs

def _cog_from_module(module):
    for cog in [c[1] for c in getmembers(module, isclass)]:
        if issubclass(cog, Cog) and cog != Cog:
            return cog