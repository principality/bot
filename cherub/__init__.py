from importlib import import_module
from inspect import getmembers, isclass
from tomlkit import loads
from pathlib import Path
from shutil import copyfileobj
from sys import argv, modules
from urllib.error import HTTPError
import urllib.request
from os import getenv
from requests import get
from shutil import rmtree
from json.decoder import JSONDecodeError
from typing import List
from superdict import SuperDict
from zipfile import ZipFile
from dataclasses import dataclass
from pyfigure import Configurable, Option

from cog import Cog, get_cogs
from utils import url_to_json
from core import get_core

core = get_core()

class Ratelimit(Exception):
    pass

class CogInstalled(Exception):
    pass

class CogNotFound(Exception):
    pass

class UpToDate(Exception):
    pass

def is_true(val):
    if val.lower() in ('y', 'yes', 't', 'true', 'on', '1'):
        return True

class Cherub(Configurable):
    """The Principality module manager."""

    config_file = 'config.toml'

    cogs = SuperDict()
    dependencies = []
    available_cogs = SuperDict()

    class Config:
        cog_data_files: List[str] = Option(['https://gitlab.com/api/v4/projects/38750529/jobs/artifacts/main/raw/cogs.json?job=run'], "A list of URLs that provide quick Cog data")
        cog_directory: str = Option('cogs/', "What directory to load modules from")

    def __init__(self):
        print('inited')

        Configurable.__init__(self)

        if getenv('GITHUB_TOKEN', None): 
            self.github_headers = {"Authorization": f"token {getenv('GITHUB_TOKEN')}"}
        else:
            self.github_headers = {}

        self.cog_directory = core.config.cog_directory

        self.populate()

        for remote in self.config.cog_data_files:
            data = url_to_json(remote, self.github_headers)
            for key, cog in data['cogs'].items():
                self.available_cogs[key] = SuperDict(cog)

    def populate(self):
        for cog_id, cog in get_cogs(self.cog_directory).items():
            if cog_id in self.cogs: continue
            self.cogs[cog_id] = cog
            if 'dependencies' in cog.metadata:
                for dep in cog.metadata['dependencies']:
                    if '@ ' in dep: dep = dep.rsplit('@ ', 1)[1]
                    self.dependencies.append(dep)

    def _save(self):
        with open('cog_requirements.txt', 'w') as file:
            file.write('\n'.join(set(self.dependencies)))

    def _not_installed(self, cog_id: str):
        cog = self.cogs.get(cog_id, None)
        if cog:
            raise CogInstalled(f"Cog '{cog.name}' is already installed")
        return cog

    def _is_available(self, cog_id: str):
        cog = self.available_cogs.get(cog_id, None)
        if not cog:
            raise CogNotFound(f"Cog '{cog_id}' does not exist")
        return cog

    def _download_cog(self, url: str, destination: Path):

        temp = Path('temp/')
        temp.mkdir(exist_ok=True)
        zip_path = temp / url.split('/')[-1]

        with open(zip_path, 'wb') as file:
            file.write( get(url, allow_redirects=True).content )

        with ZipFile(zip_path, 'r') as file:
            file.extractall(destination.parent)
        (destination.parent / zip_path.stem.replace('.zip', '')).rename(destination)

        zip_path.unlink()
        #temp.rmdir()
        return Cog.from_dir(destination)

    def install(self, cog_id: str):
        cog_id = cog_id.lower()

        self._not_installed(cog_id)

        pseudo_cog = self._is_available(cog_id)
        cog = self._download_cog(pseudo_cog['download_url'], self.cog_directory / cog_id)

        if 'dependencies' in cog.metadata:
            for dep in cog.metadata['dependencies']:
                if '@ ' in dep: dep = dep.rsplit('@ ', 1)[1]
                self.dependencies.append(dep)

        self.cogs[cog_id] = cog

    def update(self, cog_id: str):
        cog_id = cog_id.lower()

        cog = self.cogs.get(cog_id, None)
        if not cog:
            return self.install(cog_id)

        pseudo_cog = self._is_available(cog_id)
        current_version = self.cogs[cog_id].metadata.version
        available_version = pseudo_cog.latest_version
        if current_version == available_version:
            raise UpToDate(f"Cog '{cog.name}' is already up to date")

        print(current_version)
        print(available_version)
        return

        cog = self._download_cog(pseudo_cog['download_url'], self.cog_directory / cog_id)

        if 'dependencies' in cog.metadata:
            for dep in cog.metadata['dependencies']:
                if '@ ' in dep: dep = dep.rsplit('@ ', 1)[1]
                self.dependencies.append(dep)

        self.cogs[cog_id] = cog

    def delete(self, cog: str, delete_data: bool = True, delete_config: bool = False):
        cog = cog.lower()
        if not self._cog_installed(cog): return print(f"Cog '{cog}' is not installed")

        rmtree(Path(self.cogs[cog].directory).parent)
        if delete_config: Path(self.cogs[cog].config_file).unlink()
        #if delete_data:
        #    if hasattr(cog, 'db'):
        #        cog.db.clear()
        #        confirm = is_true(input("Are you sure that you want to delete the Cog's data? This cannot be reversed (y/n) "))
        #        if confirm: cog.db.clear()

        return True