from pathlib import Path
from typer import Argument
from typing import List

from . import Cherub
from core.cli import CLI

cherub = Cherub()
app = CLI()

# Pip-Based Cogs

@app.command()
def add(cog_id: str, version: str = None):
    pass

@app.command()
def remove(cog_id: str):
    pass

# Locally Stored Cogs

@app.command()
def new(
    name: str = Argument(''),
    id: str = Argument(''),
    author: str = Argument(''),
    description: str = Argument('')
):
    app.logger.pending("Creating a new cog...")

    if not name: name = app.logger.input("Name: ")

    if id and (cherub.cog_directory / id).exists():
        app.logger.error("A Cog with that ID already exists.")
        id = None
    while not id:
        id = app.logger.input("ID: ").lower()
        if (cherub.cog_directory / id).exists():
            app.logger.error("A Cog with that ID already exists.")
            id = None

    if not author: author = app.logger.input("Author: ")
    author = '[{ name = "' + author + '" }]'

    if not description: description = app.logger.input("Description: ", True)

    cwd = Path(__file__).parent
    cog_dir = cherub.cog_directory / id
    cog_dir.mkdir()

    with open(cwd / 'example' / 'pyproject.toml', 'r') as file:
        pyproject = file.read().format(name, description, author)
    pyproject_file = cog_dir / 'pyproject.toml'
    pyproject_file.write_text(pyproject)
    
    with open(cwd / 'example' / 'example.py', 'r') as file:
        code = file.read().format(name.replace(' ', ''))
    code_file = cog_dir / f'{id}.py'
    code_file.write_text(code)

    app.logger.done('Successfully created a new cog!\n')

@app.command()
def install(cogs: List[str]):
    app.logger.pending('Installing Cog(s)...')
    for cog in cogs:
        try:
            cherub.install(cog)
            app.logger.done(cog)
        except Exception as error:
            message = app.logger.error(cog)
            message.add( str(error) )
    app.logger.done('Done!')

@app.command()
def update(cogs: List[str]):
    app.logger.pending('Updating Cog(s)...')
    to_update = []
    if cogs[0].lower() == 'all':
        to_update = cherub.cogs.keys()
    else:
        to_update = cogs
    for cog in to_update:
        try:
            cherub.update(cog)
            app.logger.done(cog)
        except Exception as error:
            message = app.logger.error(cog)
            message.add( str(error) )
    app.logger.done('Done!')

@app.command()
def delete(cogs: List[str], delete_data: bool = True, delete_config: bool = False):
    for cog in cogs:
        cherub.delete(cog, delete_data, delete_config)
        print(f"Deleted cog '{cog}'")

#@app.command()
#def populate():
#    cherub.populate()
#    print(f"Populated cogs")

@app.command_group()
def list():
    pass

@list.command()
def installed():
    info = app.logger.info('Installed Cogs:')
    cogs = [c.name for c in cherub.cogs.values()] or ['None']
    for cog in cogs:
        info.add(cog)

@list.command()
def available():
    info = app.logger.info('Available Cogs:')
    available = [a for a in cherub.available_cogs if a not in cherub.cogs] or ['None']
    for cog_id in available:
        info.nl()
        cog = cherub.available_cogs[cog_id]
        info.add(f"[{cog_id}] {cog.name} Module by " + ''.join(cog.authors))
        info.add(f'"{cog.description}"')
        info.add('Tags: ' + ' '.join(cog.topics))
    info.nl()

def main():
    app()

if __name__ == '__main__':
    main()
