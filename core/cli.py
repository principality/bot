from pathlib import Path
from typer import Typer
from typer.core import TyperCommand
from typer.models import TyperInfo

from core.logger import Logger


class CLI(Typer):

    logger: Logger

    verbose: bool
    config_file = Path

    def __init__(self, *args, **kwargs):
        Typer.__init__(self, pretty_exceptions_show_locals=False, pretty_exceptions_short=True, *args, **kwargs)
        super().callback()(self._callback)
        self.logger = Logger()

    def _callback(self,
        verbose: bool = False,
        config_file: Path = Path('config.toml')
    ):
        self.verbose = verbose
        self.logger.verbose = verbose

        self.config_file = config_file

    def command_group(self,
        name: str = None,
        **kwargs
    ):
        def decorator(f):
            typer = Typer(
                name = name or f.__name__,
                **kwargs
            )
            self.registered_groups.append(TyperInfo(
                typer,
                name = name or f.__name__,
                **kwargs
                ),
            )
            return typer

        return decorator