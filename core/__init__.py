from pathlib import Path
from typing import Literal
from dotenv import load_dotenv

from pyfigure import Configurable, Config, Option
from core.logger import Logger

class Core(Configurable):

    config_file: Path
    logger: Logger
    config_directory: Path
    cog_directory: Path
    data_directory: Path

    class Config:

        config_directory: Path = Option('configs/', "What folder to store cog configuration files in")
        cog_directory: Path = Option('cogs/', "What folder to load cogs from")
        data_directory: Path = Option('data/', "What folder to store cog data in")
        default_database_type: Literal['local', 'temp', 'deta'] = Option('local', "What database type to use for cogs (Options: local, temp or deta)")

        token_env_var: str = Option('BOT_TOKEN', "The environmental variable of your bot's token. (Leave as default unless you want to work with multiple bots)")

    def __init__(self,
        verbose: bool = False,
        config_file: Path = Path('config.toml')
    ):
        global CORE
        CORE = self

        load_dotenv()
        self.config_file = Path(config_file)
        Configurable.__init__(self, manual_errors=True)
        self.logger = Logger(verbose)

        if not self.config.config_directory.exists():
            self.config.config_directory.mkdir()
        if not self.config.cog_directory.exists():
            self.config.cog_directory.mkdir()
        if not self.config.data_directory.exists():
            self.config.data_directory.mkdir()

CORE: Core = None
def get_core(*args, **kwargs):
    global CORE
    if not CORE:
        CORE = Core(*args, **kwargs)
    print(CORE)
    return CORE