from colorama import init, Fore, Style

class Message:

    text: str
    prefix: str
    color: str

    def __init__(self, text):
        self.text = text

class Info(Message):
    prefix = ' [>] '
    color = ''
class Pending(Message):
    prefix = ' [.] '
    color = Fore.YELLOW
class Done(Message):
    prefix = ' [✓] '
    color = Fore.GREEN
class Warning(Message):
    prefix = ' [!] '
    color = Fore.LIGHTRED_EX
class Error(Message):
    prefix = ' [X] '
    color = Fore.RED

class Logger:

    def __init__(self,
        verbose = False
    ):
        init() # Windows compatibility
        self.verbose = verbose

    def print(self, message, verbose = False):
        if verbose and not self.verbose:
            pass
        text = message.text
        color = message.color
        if hasattr(self, 'prefix'):
            prefix = self.prefix
        else:
            prefix = message.prefix
        if verbose:
            print(color + prefix + Fore.LIGHTBLACK_EX + text + Style.RESET_ALL)
        else:
            print(color + prefix + Style.RESET_ALL + text)
        return Reply(message, verbose=self.verbose)

    def nl(self):
        print('')

    def input(self, prompt: str, optional: bool = False):
        if optional:
            return input(Fore.YELLOW + ' * ' + Fore.BLUE + prompt + Style.RESET_ALL).strip()
        result = input(Fore.RED + ' * ' + Fore.BLUE + prompt + Style.RESET_ALL).strip()
        if not result:
            self.warning("Value cannot be empty.", True)
            return self.input(prompt)
        return result
    
    def optional_input(self, prompt):
        return input(Fore.YELLOW + ' * ' + Fore.BLUE + prompt + Style.RESET_ALL).strip()

    def info(self, text: str, verbose: bool = False):
        return self.print(Info(text), verbose)
    
    def pending(self, text: str, verbose: bool = False):
        return self.print(Pending(text), verbose)

    def done(self, text: str, verbose: bool = False):
        return self.print(Done(text), verbose)

    def warning(self, text: str, verbose: bool = False):
        return self.print(Warning(text), verbose)
    
    def error(self, text: str, verbose: bool = False):
        return self.print(Error(text), verbose)

class Reply(Logger):
    prefix = '  +  '

    def __init__(self,
        previous: Message,
        verbose: bool = False
    ):
        init() # Windows compatibility
        self.previous = type(previous)
        self.verbose = verbose
    
    def add(self, text: str, verbose: bool = False):
        return self.print(self.previous(text), verbose)