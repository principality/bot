from argparse import ArgumentError
from pathlib import Path
from hashlib import sha256
from typing import List, Set, Tuple, NamedTuple

class FileChanges(NamedTuple):
    added: Set[Path] = ()
    changed: Set[Path] = ()
    removed: Set[Path] = ()

    def all(self) -> Set[Path]:
        return self.added + self.changed + self.removed

class Overseer():

    changes = FileChanges()

    _magic_bytes = 131072

    def __init__(self,
        directory: Path = Path(),
        patterns: List[str] = ['*.*']
    ):
        self._old_hashes = {}
        self.directory = Path(directory)
        if not self.directory.is_dir():
            raise ArgumentError(directory, "Location must be a path to a directory")

        if not isinstance(patterns, list):
            patterns = [patterns]

        self._patterns = patterns
        # run check once to generate hashes for comparison
        self.check()
        # make changes empty again
        self.changes = FileChanges()

    def check(self):
        hashes = {}
        changed = set()

        # get every file in directory
        files = []
        for pattern in self._patterns:
            for file in self.directory.rglob(pattern):
                if file.is_file():
                    files.append(file)
        for file in files:
            # hash the file and store it temporarily
            file_hash = sha256()
            memview = memoryview(bytearray(self._magic_bytes))
            with open(file, 'rb', buffering=0) as file_bytes:
                for n in iter(lambda: file_bytes.readinto(memview), 0): file_hash.update(memview[:n])
            hashes[file] = file_hash.hexdigest()

            # mark the file as 'changed' if the new hash differs from the last saved hash
            if self._old_hashes.get(file) and self._old_hashes.get(file) != hashes[file]:
                changed.add(file)

        # add and subtract old and new hashes to mark files as 'added' or 'removed'
        added = set(hashes.keys()) - set(self._old_hashes.keys())
        removed = set(self._old_hashes.keys()) - set(hashes.keys())
        for r in removed:
            del self._old_hashes[r]

        # finalize
        self._old_hashes = hashes
        self.changes = FileChanges(added, changed, removed)