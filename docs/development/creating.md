# Making Your own Cogs

You can create a new cog in one of three ways:
- Running `python -m cherub new`
- Downloading the [example cog](https://gitlab.com/principality/cogs/example/-/archive/main/example-main.zip) and extracting it in your cog directory.
- Making every file manually inside of your cog directory.

Example cog file structure:

- example/
    - src/
        - [example.py](#example.py)
    - [pyproject.toml](#pyproject.toml)
    - [icon.png](#icon.png) (optional)
    - [readme.md](#readme.md) (optional)
    - [changelog.md](#changelog.md) (optional)

Each file inside of the cog's directory serves a specific purpose, explained below.

## pyproject.toml

A [PEP 621](https://peps.python.org/pep-0621/) compatible project file, required to store the cog's metadata, such as the display name, description, or version.

`pyproject.toml`
```toml
[project]
name = "Example"
description = "Example Module."
version = "1.0.0"
authors = [
    { name = "Example Creator" }
]
# optional
keywords = ["example", "basic", "learning"]
readme = "readme.md"
```

## icon.png

An image file that visually represents the cog. 

Optional, but adds eye candy.

## readme.md

A markdown file that explains the project in a more advanced format.

Optional, but useful for complicated cogs that can't be summed up in one line of the description.

## changelog.md

A markdown file documenting changes made to the cog, formatted like [this](https://keepachangelog.com/en/1.0.0/).

`changelog.md`
```md
# Changelog

## [Unreleased]
### Added
- A few things that haven't been pushed yet.

## [1.0.0] - 2017-06-20
### Added
- The new cog.
### Changed
- Your principality bot.
### Removed
- The void where this cog used to be.
```

Optional, but helps people learn how the cog has evolved.

## example.py

A cog is initiated by making a new class that inherits the `cog.Cog` class.

```py
from cog import Cog

class Example(Cog):
    pass
```

> Note: Only the first found `Cog` class will be loaded.

To give the cog functionality, you can use:
- [Configuration](#configuration)
- [Databases](#databases)
- [Load Method](#load-method)
- [Ready Method](#ready-method)
- [Unload Method](#unload-method)
- [Listeners](#listeners)
- [Commands](#commands)

## Configuration

Cogs are configurable using the [pyfigure](https://pypi.org/project/pyfigure/) library, which means that all you need to make configurable variables is a `Config` class inside of the cog.

```py
class Config:
    is_true: bool = ConfigOption(True, "Whether or not this value is true")
```

> You can only have one `Config` class, but you can make new classes in the `Config` class to make nested config options.

```py
class Config:
    main_value: str = ConfigOption('main', "The main configuration value")
    class sub_config:
        sub_value: str = ConfigOption('main', "A secondary configuration value")
```

Users can then change the value of the config value inside of the config directory (`configs/` by default), in the cog's file (`configs/Example.toml` in this case).

> Values are automatically type checked and parsed by the library.

When the cog is loaded, it'll have a new attribute named `config` (accessible with `self.config`), which, in this case, returns `{'is_true': True}` by default, but changes depending on what the user changed it into.

> Config values can also be accessed as an attribute. (Including subvalues)
```py
>>> this.config['is_true']
# True
>>> this.config.is_true
# True
```

> For more information about configuration files, visit the [pyfigure](https://pypi.org/project/pyfigure/) documentation.

## Databases

Persistent data storage is possible using the cog's `Cog.db` attribute, which is a custom `Database` object with 2 attributes:

- `Database.data`
    - A dictionary object that gets saved automatically.
- `Database.files`
    - A `Files` object with methods to save and load files. Optionally, you can use it as if it were a dictionary, where any keys are the file path, and values are the file's bytes.

## Load Method

The `Cog.load()` method is called once the bot has loaded the cog.

```py
def load(self):
    print('Loaded!')
```

This method is called before the bot is async ready, so use the `Cog.ready()` method in case you want to initiate the cog with information only accessible by the bot.

## Ready Method

The `Cog.ready()` method is called once the bot is fully loaded and async ready, giving you access to data only accessible by bot such as member roles or server members.

```py
async def ready(self):
    print('Async Loaded!')
```

## Unload Method

The `Cog.unload()` method is called when the cog is either manually disabled, or the shuts off.

> Note: This method isn't guaranteed to always fire, in the case of a power outage or some hardware problem.

```py
def unload(self):
    print('Async Loaded!')
```

## Listeners

Listeners are methods, with the `Cog.listener()` decorator, that get called when a certain event happens on Discord.

The event which is listened for is defined as the method's name by default, but can be overrided by adding a string to the method's decorator.

```py
@Cog.listener()
async def on_member_join(self, member):
    print(f"{member.mention} has joined {member.guild.name}")
```

Or:

```py
@Cog.listener('on_member_join')
async def my_listener(self, member):
    print(f"{member.mention} has joined {member.guild.name}")
```

> Note, the arguments of the listener method change based on which event is being listened for.

> A list of events can be found [here](https://docs.nextcord.dev/en/stable/api.html#discord-api-events).

## Commands

Commands can either be [slash commands](https://docs.nextcord.dev/en/stable/interactions.html?highlight=slash%20commands#simple-slash-command-example), [user commands](https://docs.nextcord.dev/en/stable/interactions.html?highlight=slash%20commands#user-commands) or [message commands](https://docs.nextcord.dev/en/stable/interactions.html?highlight=slash%20commands#message-commands), all accessible using the `Cog.slash_command()`, `Cog.user_command()` and `Cog.message_command()` decorators accordingly.

> Only slash commands will be covered, and will be referred to as just commands.

> You can also use the `Cog.command()` decorator as a shorthand for `Cog.slash_command()`

Commands are methods, decorated with `Cog.command()`, that get called whenever a user initiates a command with the same name as the command.

> The name of the command is the method's name by default, but can be changed by adding `name='command_name'` to the decorator's arguments.

```py
@Cog.command()
async def ping(self, ctx: Interaction):
    await ctx.response.send_message("Pong!")
```

The `ctx` argument is a Nextcord [`Interaction`](https://docs.nextcord.dev/en/stable/api.html?highlight=interaction#nextcord.Interaction) object, which contains data about the command's execution.

Usually, you'd use it to reply to the interaction, with `ctx.send()`.

> If you want the reply to only be visible to the person who sent the command, add `ephemeral=True` as an argument in `ctx.send()`.

`example.py`
```py
from cog import Cog, ConfigOption, SlashOption

class Example(Cog):

    # Built-in attributes:
    # self.config - A dictionary containing the configuration values of the cog
    # self.db - The Database object of the cog, for persistent data
    # self.directory - The Path of the folder containing this cog
    # self.metadata - A dictionary containing meta data about the cog, from pyproject.toml

    class Config:
        is_true: bool = ConfigOption(default=True, description="Whether or not this value is true")

    def load(self):
        print('Loaded!')

    async def ready(self):
        print('Async Loaded!')
    
    def unload(self):
        print('Unloaded!')

    @Cog.command()
    async def ping(self, ctx: Interaction):
        await ctx.send("Pong!")

    @Cog.listener()
    async def on_member_join(self, member):
        print(f"{member.mention} has joined {member.guild.name}")
```