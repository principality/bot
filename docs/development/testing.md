# Testing Your Cogs

Principality has a development mode that can be toggled by adding `--dev` at the end of the bot starting command.

Development mode automatically reloads your cogs whenever you make a change to them or any files inside of their directories.