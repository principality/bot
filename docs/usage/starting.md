# Requirements

To get started with making a Principality Bot, you must first have:
- [Python](https://www.python.org/downloads/)
- Basic Computer Usage Knowledge

> Validate that Python is installed by running `python --version` in your command prompt.

> If you get an error message saying `'python' is not recognized as an internal or external command, operable program or batch file.` then you must download and install python from [here](https://www.python.org/downloads/).

term
: definition

# Getting Started

First, download the [Example Bot](https://gitlab.com/principality/example/-/archive/main/example-main.zip) package and extract it.

Open your command prompt inside of the folder and run the following commands:

`pip install -r requirements.txt`
- This installs the code needed to run Principality and Cherub.

`python -m principality start`
- This starts the bot for the first time, generating additional files.
  > You can close the bot by clicking `ctrl + c` on your keyboard
- This is to validate that the package is correctly installed

Congratulations! You just made your own Principality Bot.

Next up, learn how to [Using Cogs](docs/usage/cogs.md) guide.