# Requirements

To add cogs to your Principality Bot, you must first have:
- [Python](https://www.python.org/downloads/)
- Basic Computer Usage Knowledge

# Adding Cogs

View available cogs by running `python -m cherub list available`

> For additional information about a specific cog, you can run `python -m cherub info (cog)` (W.I.P.)

To install cogs, run `python -m cherub install (cog)`

The cog should be loaded the next time you start your Principality Bot.

# Updating Cogs

To update cogs, run `python -m cherub update (cog)`, or `python -m cherub update all` to automatically update every cog.

> For an easy way to track cog updates, check the [#cog-updates](https://example.com) channel of Principality's Discord server.

# Deleting Cogs

To delete cogs, run `python -m cherub delete (cog)`