<p align="center">
  <img src="https://gitlab.com/principality/data/-/raw/main/assets/banner.png">
</p>

---

**Principality** is a Discord Bot framework made in Python using the [Nextcord](https://nextcord.dev/) library.

By itself, your **Principality** bot is functionless, its main purpose is to load **Cogs**, which are modules which add different functions to your bot.

These can be anything from [commands for rolling die](W.I.P.) to [listeners that pin a chat message if it gets enough reactions](W.I.P.).

# Usage

To make your own **Principality** bot, you should read the [Getting Started](docs/usage/starting.md) guide.

To then add functionality to your bot, you should read the [Using Cogs](docs/usage/cogs.md) guide.

To configure your installed cogs, read the [Configuring Cogs](docs/usage/configuring.md) guide. **(W.I.P.)**

To host your bot on the cloud for free, read the [Bot Hosting](docs/usage/hosting.md) guide. **(W.I.P.)**

# Development

In case you're interested in creating your own cogs for **Principality**, read the [Cog Creation](docs/development/creating.md) guide.

To easily share your cogs, read the [Cog Repositories](docs/development/repo.md) guide.